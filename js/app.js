$(function(){
/*  
	HIERARCHY
	body
	-- container
	-- -- row
	-- -- -- navigation
	-- -- -- canvas
	-- -- -- -- panels
	-- -- -- -- -- row
	-- -- -- -- -- row flexible   (take the difference of space btw panelsHeight - rowsHeaight )
	-- -- -- -- -- -- scroll-pane
	-- -- -- -- -- row

	active_panel = panel where I show actions
	primary = panels with actions

*/

var image_path = "img/";
var WINDOW_H = $(window).height();
var WINDOW_W = $(window).width();


window.onresize = function(event) {
    WINDOW_H = $(window).height();
	WINDOW_W = $(window).width();
	can.update();
	nav.update();
	dashboard.update();
	offer_card.update();
	title_list.update();
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////// SIERRA
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	var sierra = {
		init: function (){
			console.log("INIT sierra");
			this.children = new Array(); //list of obj
			this.id = "#container .row";
		},
		set: function(){
			console.log("SET sierra");
			//
			nav.init();
			can.init();
			dashboard.init();
			//offer_card.init();
			//title_list.init();
		},
		update: function(){},
		get: function(){
			console.log("GET sierra");
			return {
			}
		}
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////// WIDGETS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	//////// NAV
	////////////////////////////////////////////////////////////////////////
	var nav = {
		init: function (){
			console.log("INIT nav");
			this.reset();
			this.id = "navigation";
			this.stagged = false;  //elem not added to page
			this.expanded = true;
			this.widthClass = "";
			this.parent = sierra;
			var that = this;   //just to be use inside ajax function
			//load html template and set var
			setTemplate(this);	
		},
		set: function(){
			console.log("SET navigation"); 
			addElem(this); //parent   stage element    //can = parent
			//setHeightRow(this);
			//moveOut(this,false,0); //first time move out immediatly // remove == false don't remove
			//moveIn(this);
			$(this.id).height(WINDOW_H);
			this.interaction();
			this.update(0);
			$(this.id+" .hub_link").addClass("active");
		},
		get: function(){
			console.log("GET nav");
			return {
				width: $(this.id).width(),

			}
		},
		update:function(time){
			var span_class="";
			if(this.expanded){
				span_class = "span2";
			}
			else{
				span_class = "span1";
				$(this.id+" .text").addClass("hide");
			}
			var html = '<div class="'+span_class+'"></div>'
			$("body").append(html);
			this.widthClass = $("."+span_class).width();
			$(html).remove();
			anim.updatePanel(this.id,time,0,0,this.widthClass,WINDOW_H);
			var that = this;
			setTimeout(function(){
				if(that.expanded){
					$(that.id).addClass("span2").removeClass("span1");
					$(that.id+" .text").removeClass("hide");
				}
				else{
					$(that.id).addClass("span1").removeClass("span2");
					
				}

			},time*1000-2);
		},
		interaction: function(){
			var that = this;
			
			$(this.id + " a[href='#new_offer'] ").click(function(){
				$(that.id+" li.active").removeClass("active");
				$(this).parent().addClass("active");
				that.expanded = false;
				that.update(0.2);
				changeSection();
				offer_card.init();
			});
			$(this.id + " a[href='#my_hub'] ").click(function(){
				$(that.id+" li.active").removeClass("active");
				$(this).parent().addClass("active");
				that.expanded = true;
				that.update(0.2);
				changeSection();
				dashboard.init();
			})
		},
		reset: function(){
			this.id = null;
			this.html = null;
			this.stagged = false;  //elem not added to page
			this.parent = null;
		}
	}
	//////////////////////////////////////////////////////////////////////////
	//////// CANVAS
	////////////////////////////////////////////////////////////////////////
	var can = {
		init: function(){
			console.log("INIT canvas");
			this.reset();
			this.id = "canvas";
			this.html = '<div id="'+this.id+'" ></div>';
			this.id = "#"+this.id;
			this.children = new Array();
			this.parent = sierra;
			this.placement = "#container .row";
			this.set();
		},
		set: function(){
			//console.log("SET canvas");
			addElem(this);
			this.update();
		},
		get: function(){
			return {
				id: this.id,
				width: $(this.id).width()
			};
		},
		update: function (){
			//console.log("++++++++++++++++UPDATE canvas");
			var nav_w = 0;
			nav_w = nav.widthClass; //to calculate width with padding, no have margin as well outerWidth(true)
			var canW = 0;
			var canL = 0;   

			for (var i in this.children)
			{
	    		canW += $(this.children[i].id).outerWidth();
	    		console.log("CANVAS CONT "+canW + " "+ this.children[i].id);
			}
			canW+=100;

			if(canW < (WINDOW_W - nav_w)) //is possible to show all canvas into the page
				{ canL = nav_w; canW = WINDOW_W - nav_w;}
			else
				{ canL = WINDOW_W - canW; }
			// if(WINDOW_W > canW)
			// 	canL = Math.abs(canL);
			// else
			// 	canL = - Math.abs(canL);
			console.log("left"+canL+" width:"+canW+"WINDOW W:"+WINDOW_W);
			anim.updatePanel(this.id,0.5,canL,0,canW,WINDOW_H);  //(obj,time,_l,_t,_w)
			$(can.id).height(WINDOW_H);
			setActivePanel();
		},
		removeChildren: function(obj){

		},
		reset: function(){
			this.id = null;
			this.html = null;
			this.stagged = false;  //elem not added to page
			this.parent = null;
		}
	}
	//////////////////////////////////////////////////////////////////////////
	//////// DASHBOARD 
	////////////////////////////////////////////////////////////////////////
	var dashboard = {
		init: function (){
			console.log("INIT dashboard");
			this.reset();
			this.id = "dashboard";
			this.stagged = false;  //elem not added to page
			this.parent = can;
			var that = this;   //just to be use inside ajax function
			//load html template and set var
			setTemplate(this);	
		},
		set: function(){
			console.log("SET dashboard"); 
			addElem(this); //parent   stage element    //can = parent
			setHeightRow(this);
			moveOut(this,false,0); //first time move out immediatly // remove == false don't remove
			moveIn(this);
			this.interaction();
		},
		get: function(){
			console.log("GET dashboard");
			return {
			}
		},
		update: function(){
			setHeightRow(this);
		},
		interaction: function(){

		},
		reset: function(){
			this.id = null;
			this.html = null;
			this.stagged = false;  //elem not added to page
			this.parent = null;
		}
	};
	//////////////////////////////////////////////////////////////////////////
	//////// OFFER CARD
	////////////////////////////////////////////////////////////////////////
	var offer_card = {
		init: function (){
			console.log("INIT offer_card");
			this.reset();
			this.id = "offer_card";
			this.stagged = false;  //elem not added to page
			this.parent = can;
			var that = this;   //just to be use inside ajax function
			//load html template and set var
			setTemplate(this);	
		},
		set: function(){
			console.log("SET offer_card"); 
			addElem(this); //parent   stage element    //can = parent
			setHeightRow(this);
			moveOut(this,false,0); //first time move out immediatly // remove == false don't remove
			moveIn(this);
			this.interaction();
		},
		get: function(){
			console.log("GET offer_card");
			return {
			}
		},
		update: function(){
			setHeightRow(this);
		},
		interaction: function(){
			var that = this;
			$(this.id+" li a[href='#select_titles']").click(function(){
				$(this).parent().addClass("selected");
				if(title_list.stagged){
					//offer_card.set();
					//removeElemOnRight(that)
				}	
				else{
					//offer_card.set();
					title_list.init();	
				}
				can.update();
			})
			$(this.id).click(function(){
				// if(title_list.stagged){
				// 	removeElemOnRight(that);
				// 	can.update();
				// }

			});	
		},
		updateFields: function(){
			var that = this;
			var string = "";
			this.titles_selected = new Array();

			for(i in table_elem){
				if(table_elem[i].selected == true)
					this.titles_selected.push(table_elem[i]);
			}
			if(this.titles_selected.length == 0) //no titles selected reset defaul value
				string = 'all titles >';
			//$(that.id+" a#select_titles text").html('data-original-title',string);
			string = string.substring(0, 20)+'... >';
			console.log($("#link_select_titles a .text"))
			$("#link_select_titles a .text").text(string);
			console.log("UNSELECTED");
			$(this.id+" li").removeClass("selected");
		},
		reset: function(){
			this.id = null;
			this.html = null;
			this.stagged = false;  //elem not added to page
			this.parent = null;
		}
	};
	//////////////////////////////////////////////////////////////////////////
	//////// SUB LIST
	////////////////////////////////////////////////////////////////////////
	var title_list = {
		init: function (){
			console.log("INIT title_list");
			this.reset();
			this.id = "title_list";
			this.stagged = false;  //elem not added to page
			this.parent = can;
			var that = this;   //just to be use inside ajax function
			//load html template and set var
			this.item_selected = 0;	
			setTemplate(this);

				
		},
		set: function(){
			console.log("SET title_list"); 
			
			addElem(this); //parent   stage element    //can = parent
			setHeightRow(this);
			moveOut(this,false,0); //first time move out immediatly // remove == false don't remove
			moveIn(this);
			this.interaction();
			anim.closeSelectionPanel(this,0);
			table.addTableToList();
			infiniteScrolling();
			this.setTray();
		},
		update: function(){
			setHeightRow(this);
		},
		get: function(){
		},
		addColumn: function(filter){
			console.log("add column");
			//$(".list").append("<ul class='sales column span'></ul>")
			var cat;
			var label;
			switch(filter){
				case'title':
					cat = catalog.titles;break;
				case'actor':
					cat = catalog.actors;break;
				case'sale':
					cat = catalog.sales;break;
			}
			$("#head_list table thead tr >th").width($("tr .cover_title").width());
			$("#head_list table thead tr").append("<th data="+cat.filter.name+">"+cat.table_header.caption+"</th>");

			cat = cat.cell;
			for(var i in cat){
				$(".asset[order='"+i+"']").append(cat[i]);
			}
			
		},
		removeColumn: function(filter){
			console.log("add column");
			//$(".list").append("<ul class='sales column span'></ul>")
			$(".asset .cell_"+filter).remove();
			$("#head_list table thead tr th[data="+filter+"]").remove();

		},
		interaction: function(){
			var that = this;
			$(this.id+" #filters").click(function(){	
				can.update();
			})
			//catalog
			$(this.id+" #list tr.asset").live('click',function(){
				console.log("click on asset");
				if($(this).hasClass("selected") ){
					$(this).removeClass("selected");
					console.log("-----"+this);
					that.removeSelectedItem($(this).attr("order"));
				}
				else{
					$(this).addClass("selected");
					//console.log("-----");
					that.addSelectedItem($(this).attr("order"));
				}		
				that.updateTrayMidClose(that.checkSelection());		
			});
			///selection bucket
			$(this.id+" #selection .titles .icon-remove-sign").live('click',function(){
				console.log("remove "+this);
				that.removeSelectedItem(this);
			});
			$(this.id+" #selection .top_bar").click(function(){
				if($(that.id+" #selection").hasClass("mid")){
					anim.openSelectionPanel(that,0.5);
					console.log("in panel");
				}
					
				else if($(that.id+" #selection").hasClass("in")){
					anim.midSelectionPanel(that,0.5);
					console.log("mid panel");
				}	
			});
			$(this.id+" .actions_bar a[href='#selectAll']").live('click',function(){
				$(that.id+" #list .asset").each(function(){
					if(!$(this).hasClass("hide")){
						$(this).addClass("selected");
						that.addSelectedItem($(this).attr("order"));
					}
				})
				that.updateTrayMidClose(that.checkSelection());		
			});
			////////TRAY
			$(this.id+" #selection a[href='#deselectAll']").live('click',function(){
				$(that.id+" #list .asset").each(function(){
					$(this).removeClass("selected");
					that.removeSelectedItem($(this).attr("order"));
				})
				that.updateTrayMidClose(that.checkSelection());	
			});
			$(this.id+" #selection a[href='#remove_from_tray']").live('click',function(){
				console.log("remove from tray"+$(this).attr("order"));
				that.removeSelectedItem($(this).attr("order"));
				$('#list .asset[order='+$(this).attr("order")+']').removeClass("selected");
				that.updateTrayOpenClose(that.checkSelection());	
			});
			//filters
			// $(this.id+" #fields .eye").click(function(){
			// 	//alert("show"+this.checked+$(this).attr("filter"));
			// 	var f = $(this).attr("name")
			// 	if(this.checked){
			// 		that.addColumn(f);
			// 		if($(this).attr("graph") && (!graph.stagged)){
			// 			//graph.init();
			// 			can.update();
			// 		}
			// 	}
			// 	else{
			// 		that.removeColumn(f);
			// 		if(graph.stagged){
			// 			moveOut(graph,true);
			// 			can.update();
			// 		}
			// 	}
			// });
			//////SUBMIT SELECTION   			//MODAL + CLOSE PANEL
			$(this.id+" .actions_bar a[href='#submit_selection']").click(function(){
				if(that.item_selected == 0){
					offer_card.updateFields();
					moveOut(title_list,true);
					$('#modal_title_list').modal('hide');
				}
				else{
					$('#modal_title_list').modal('show');
					$('#modal_title_list #modal_title_list_label').html('Your selection');
					$('#modal_title_list .modal-body').html('A selection of '+that.item_selected+' items will be created <div class="row"><input type="checkbox" name="vehicle" value="dynamic" >Dynamic package</div>');
					$('#modal_title_list .modal-footer').html('<a class="btn" aria-hidden="true"  data-dismiss="modal" >Dismiss</a><a class="btn btn-primary" href="#save_sel">Save Selection</a>');
				}
			});
			$("#modal_title_list a[href='#close_panel']").live('click', function(){
				console.log("+++CLOSE PANEL")
				$('#modal_title_list').modal('hide');
				if(that.item_selected > 0)
				{
					//offer_card.updateFields();
					moveOut(title_list,true);
				}
				if(title_list.stagged == true){
					//offer_card.updateFields();
					moveOut(title_list,true);	
				}
			});
			$("#modal_title_list a[href='#save_sel']").live('click', function(){
				console.log("+++CLOSE PANEL")
				$('#modal_title_list').modal('hide');
				if(that.item_selected > 0)
				{
					moveOut(title_list,true);
				}
				if(title_list.stagged == true){
					moveOut(title_list,true);	
				}
				offer_card.updateFields();
			});

			//////HEADER
			$(this.id+" .actions_bar a[href='#close_selection']").click(function(){
				//console.log("Close Panel");
				if(that.item_selected == 0){
					that.closePanel();
					$('#modal_title_list').modal('hide');
				}
				else{
					$('#modal_title_list').modal('show');
					$('#modal_title_list #modal_title_list_label').html('Cancel your selection?');
					$('#modal_title_list .modal-body').html("Do you want to close the panel and lose your selection?");
					$('#modal_title_list .modal-footer').html('<a class="btn" aria-hidden="true"  data-dismiss="modal">Dismiss</a><a class="btn btn-primary" href="#close_panel">Close panel</a>');
				}
			});
			// $(this.id+" a[href='#cover_format']").click(function(){
			// 	var i=0;
			// 	var cover = $(that.id+" #list .asset .cover_title").find(".cell_"+catalog.covers.id);
			// 	console.log($(cover).attr('class'));
			// 	if($(cover).attr('class') == undefined) // if cover already exist don't do anything exit
			// 	{

			// 		$(that.id+" #list .asset").each(function(){
			// 			$("tr.asset[order='"+i+"'] .cover_title .cell_title").before(table_elem[i]);
			// 			i++;
			// 		});
			// 	}
				
			// });
			// $(this.id+" a[href='#list_format']").click(function(){
			// 	var i=0;
			// 	$(that.id+" #list .asset").each(function(){
			// 		$("tr.asset[order='"+i+"'] .cover_title").find(".cell_cover").remove();
			// 		i++;
			// 	});
			// });
		},
		setTray : function(){
			var c = 0;
			for( i in offer_card.titles_selected){
				console.log(offer_card.titles_selected[i]);
				this.addSelectedItem(offer_card.titles_selected[i].index);
			}
			this.updateTrayOpenClose(this.checkSelection());
		},
		checkSelection: function(){   /// check what was the prev selection that one in offer_card
			var c = $(this.id+" #list table tbody").children("tr.selected").length;
			$(this.id+" #selection .top_bar .caption p").html(c+" Titles selected");
			console.log("======"+c);
			return c;
		},
		updateTrayMidClose : function(c){
			if(c==0){
				anim.closeSelectionPanel(this,1);
			}
			else{
				anim.midSelectionPanel(this,1);
			}
		},
		updateTrayOpenClose : function(c){
			if(c==0){
				anim.closeSelectionPanel(this,1);
			}
			else{
				anim.openSelectionPanel(this,1);
			}
		},
		addSelectedItem: function(index){
			console.log("add index "+index);
			table_elem[index].selected = true;
			this.item_selected++;
			var $elem = $(this.id+" #list table tbody tr[order="+index+"]");
			if(!$elem.hasClass("selected"))
				$elem.addClass("selected");
			var b = '<tr class="row asset" order="'+index+'"><td>'+table_elem[index].cover.html+'</td><td>'+table_elem[index].title.html+'</td><td class="remove_asset"><a  href="#remove_from_tray" order="'+index+'"><i class="icon-remove-circle"></td></tr>';
			console.log(b);
			$(this.id+" #selection .titles table tbody").append(b);
		},
		removeSelectedItem: function(index){
			console.log("remove index "+index);
			table_elem[index].selected = false;
			this.item_selected--;
			var r = $(this.id+" #selection .titles table tbody tr[order='"+index+"']");
			r.remove();
		},
		updateSelection: function(){
			var c = $(this.id+" #selection .titles table tbody").children(".asset").length;
			if(c==0){
				anim.closeSelectionPanel(this,0.5);
			}
			else {
				anim.midSelectionPanel(this,0.5);
			}
			this.item_selected = c;
			console.log(this.item_selected);
			$(this.id+" #selection .top_bar .caption p").html(c+" Titles selected");
		},
		closePanel: function(){
			offer_card.updateFields();
			moveOut(title_list,true);
		},
		reset: function(){
			$(this.id).die('click'); 
			this.id = null;
			this.html = null;
			this.stagged = false;  //elem not added to page
			this.parent = null;

		}
	};
	//////////////////////////////////////////////////////////////////////////
	//////// GRAPH
	////////////////////////////////////////////////////////////////////////
	var graph = {
		init: function (){
			console.log("INIT graph");
			this.reset();
			this.id = "graph";
			this.stagged = false;  //elem not added to page
			this.parent = can;
			var that = this;   //just to be use inside ajax function
			//load html template and set var
			this.item_selected = 0;	
			setTemplate(this);	
				
		},
		set: function(){
			console.log("SET graph"); 
			addElem(this); //parent   stage element    //can = parent
			setHeightRow(this);
			moveOut(this,false,0); //first time move out immediatly // remove == false don't remove
			moveIn(this);
			this.interaction();
		},
		get: function(){
			console.log("GET graph");
			return {
			}
		},
		interaction: function(){
		},
		reset: function(){
			this.id = null;
			this.html = null;
			this.stagged = false;  //elem not added to page
			this.parent = null;
		}
	};
	//////////////////////////////////////////////////////////////////////////
	//////// ANIM
	////////////////////////////////////////////////////////////////////////
	var anim = {
		panelIn: function(obj,time){
			var time = typeof time !== 'undefined' ? time : 0.5;
			console.log("MOVE IN "+obj.id+" to "+ 0 + "in "+ time+" s");
			TweenLite.to($(obj.id), time, {css:{left: "0px",opacity:1}});
		},
		panelOut: function(obj,time){
			var time = typeof time !== 'undefined' ? time : 0.5;
			//anim pos exit right side
			//opacity 0
			console.log("MOVE OUT "+obj.id+" to "+ WINDOW_W+ " in "+ time+" s");
			TweenLite.to($(obj.id), time, {css:{left:WINDOW_W,opacity:0},onComplete:removeElem(obj)});
		},
		updatePanel: function(obj,time,_l,_t,_w,_h){
			var time = typeof time !== 'undefined' ? time : 0.5;
			var _t = typeof _t !== 'undefined' ? _t : 0;
			var _w = typeof _t !== 'undefined' ? _w : "100%";
			console.log("UPDATE "+obj +" time "+time+" l "+_l+" t "+_t+" w "+_w);
			TweenLite.to($(obj), time, {css:{left:_l, top:_t,width: _w,height:_h}});
		},
		closeSelectionPanel: function (obj,time,_t){
			var t = $(obj.id).height();
			$(obj.id+" #selection").addClass("out").removeClass("in mid");
			this.moveSelectionPanel(obj.id+" #selection",time,t);
			TweenLite.to($(obj.id+" #list table"), time, {css:{opacity:1}});
		},
		openSelectionPanel: function (obj,time,_t){
			var t = $(obj.id).height() - $(obj.id+" #selection").height() - $(obj.id+" .actions_bar").height();
			console.log(t);
			$(obj.id+" #selection").addClass("in").removeClass("mid out");
			this.moveSelectionPanel(obj.id+" #selection",time,t);
			TweenLite.to($(obj.id+" #list table"), time, {css:{opacity:0.5}});
		},
		midSelectionPanel: function (obj,time,_t){
			var t = $(obj.id).height() - $(obj.id+" .actions_bar").height() - $(obj.id+" #selection .top_bar").height();
			$(obj.id+" #selection").addClass("mid").removeClass("in out");
			this.moveSelectionPanel(obj.id+" #selection",time,t);
			TweenLite.to($(obj.id+" #list table"), time, {css:{opacity:1}});
		},
		moveSelectionPanel: function (obj,time,_t){
			var time = typeof time !== 'undefined' ? time : 0.5;
			TweenLite.to($(obj), time, {css:{top:_t}});
		}

	}
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// // TABLE
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	var db = {}
	db.covers = new Array(image_path+"title_cover_1.jpg",image_path+"title_cover_2.jpg",image_path+"title_cover_3.jpg",image_path+"title_cover_4.jpg",image_path+"title_cover_5.jpg");
	    
	db.titles = new Array("The Shawshank Redemption (1994)","The Godfather (1972)","The Godfather: Part II (1974)","The Good, the Bad and the Ugly (1966)","12 Angry Men (1957)","The Dark Knight (2008)","Schindler's List (1993)","Fight Club (1999)","Star Wars: Episode V - The Empire Strikes Back (1980)","Batman (2012)");
	db.sales = new Array(0,10,20,30,40,50,60,70,80,90,100);

	var table_elem = new Array(); //contains the list of elements

	var table = {
		buildTableDb :  function(){
			for(var i = 0 ; i < 20; i++){
			    this.addElemTable(i);
			}
		},
		addElemTable: function(i){
		    var t = Math.floor((Math.random()*db.titles.length));
		    var c = Math.floor((Math.random()*db.covers.length));
		    console.log(db.titles[t]);
		    var elem = {
		        index: i,
		        selected: false,
		        visible: true,
		    }
		    elem.title = {
		        id: "title",
		        value: db.titles[t],
		        html: '<div class="cell span cell_title" ><div class="span text" value="'+db.titles[t]+'"><p class="teal">'+db.titles[t]+'</p></div></div>',
		        visible: true
		    }
		    elem.cover = {
		        id: "cover",
		        value: db.covers[c],
		        html: '<div class="cell span cell_cover" ><div class="span"><img width="88" height="59" src="'+db.covers[c]+'"/></div></div>',
		        visible: false
		    }
		    elem.sales = {
		        value: "",
		        html: "",
		        visible: false
		    }
		    //console.log(db.titles[t]);
		    table_elem.push(elem);
		},
		addTableToList : function(){
			$("#head_list").append('<table  class="table table-condensed"></table>');
			for(i in table_elem){
				this.addItemList(i);
			}
		},
		addItemList : function(i){	
		    var td = "<td class='cover_title'>"+table_elem[i].cover.html+table_elem[i].title.html+"</td>";
		    var html = '<tr class="row asset" order="'+i+'">'+td+'</tr>';
		    $("#list tbody").append(html);
		    //console.log("td "+html);
		}
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////// GENERAL FUNCTIONS
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function moveIn(obj,t){
		anim.panelIn(obj,t);
	}
	function moveOut(obj,remove,t){
		if(remove && obj.stagged==true){
			obj.stagged = false;  //elem should be removed from page
			removeElemOnRight(obj);
		}
		anim.panelOut(obj,t); //first time move out immediatly
	}
	function addElem(obj){
		obj.stagged = true;
		console.log("PLACED HTML elem"+obj.id+" to "+ obj.placement);
		$(obj.placement).append(obj.html);
		if(obj.parent)
			addChildren(obj);
		obj.parent.update();

	}
	function removeElem(obj){
		if(!obj.stagged){ // if elem not part of the page remove
			console.log("REMOVE elem"+obj.id+" from "+ obj.placement);
			var parent = obj.parent;
			$(obj.id).remove();
			if(parent)
				removeChildren(obj);
			console.log(obj.id);
			console.log(parent.id);
			parent.update();
			obj.reset();  // reset variables
		}
	}
	function addChildren(obj){
		console.log("ADD CHILDREN "+ obj.id +" to "+ obj.parent.id);
		obj.parent.children.push(obj);
		console.log(obj.parent.children);
	}
	function removeChildren(obj,i){
		console.log("REMOVE CHILDREN "+ obj.id +" from "+ obj.parent.id);
		var index = obj.parent.children.indexOf(obj);
		obj.parent.children.splice(index,1);
		console.log(obj.parent.children);
	}
	
	function changeSection(){
		//loop all element inside div canvas
		//start from the last and remove them all
		//when finish add new section
		var elem = new Array();
		for (var i in can.children){
			elem.push(can.children[i]);
		}
		for (var i in elem){
			console.log(elem[i].id);
			moveOut(elem[i],true);

		}
	}
	function removeElemOnRight(obj){
		var index = can.children.indexOf(obj);

		var elem = new Array();
		for (var i  in can.children){
			if(i > index )
				elem.push(can.children[i]);
		}
		for (var i in elem){
			console.log(elem[i].id);
			moveOut(elem[i],true);

		}
	}
	function setTemplate(obj){
		var that = obj;
		$.ajax({
			url: that.id+".html",
			dataType:"html",
			cache: false,
			success: function(html){
				var newdata = {
					component_id: that.id,
					image_path: "img/"+that.id+"_"
				}
				for(var key in newdata){
					var re = new RegExp(RegExp.quote("["+key+"]"), "g");
					var html = html.replace(re, newdata[key]);
				}	
				that.html = html;
				that.id = "#"+that.id;
				//console.log(html);
				that.placement = that.parent.id;    //#canvas
				that.set();
			},
			error: function(){
				console.log("error loading");
			}
		});		
	}
	function setActivePanel(){
		var last = $("#canvas").children(".panel.primary").length -1;
		var i=0;
		$("#canvas").children(".panel.primary").each(function(){
			if(i==last)
			{
				$(this).addClass("active_panel").removeClass("inactive_panel");
			}
			else
			{
				$(this).addClass("inactive_panel").removeClass("active_panel");
			}
			i++;
		})		
	}
	function setHeightRow(panel,h){
		//console.log("SET HEIGHT======================"+ "#"+panel.id+" >.row");
		var panelH = $(panel.id).height();
		var rowH = 0;
		$(panel.id+" >.row").each(function(){

			if(!$(this).hasClass('flexible')){
				rowH +=$(this).height(); 
			}	
		});
		$(panel.id+" >.row.flexible").height(panelH-rowH);
		//console.log("======================"+rowH);		
		
	}
	RegExp.quote = function(str) {
    	return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
 	}
 	infiniteScrolling = function(){
 		$("#list .scroll-pane").scroll(function() {
	        console.log("scroll Y"+$("#list .scroll-pane").scrollTop());

	        if (($("#list .scroll-pane").scrollTop()) + $("#list .scroll-pane").height() >= $("#list .scroll-pane table").height() - 50) {
	            
	            var i = $("#list tbody").children('.asset').length;

	            if(i < 200){
	            	console.log("scroll")
	                table.addElemTable(i);
	                table.addItemList(i);
	             }
	        }
	    });
 	}

 	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////// LAUNCH PROGRAM
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	sierra.init();
	table.buildTableDb();
	sierra.set();
	$('.scroll-pane').jScrollPane();

	//$(".nano").nanoScroller();
});

function searchELemInList(text){
        var t;
        var e;
        var s;
        
        console.log("+++++++++++++++++++++++++++++++")

       
        var text = text.value.toLowerCase();

        var n = $("tr.asset").children().length;
        for(var j=0; j<n;j++){  //loop elements in table

            var s = $("#list").find("tr[order='"+j+"'] .cell_title .text").attr('value');
            var boldCopy = new Array();

            var c = 0;
            var char_found = new Array();
            for(var z=0 in s){ //loop string title of single elem
                
                boldCopy.push(s[z]);
                s[z] = s[z];

                var index = null; //where the first char is been found
                for(var i=0 in text){  //char by char of the input
                    t = text[i]; 
                    
                    if(s[z].toLowerCase() == t.toLowerCase() ){
                        var w = 0;
                        //console.log("s[z] ="+s[z]+"  "+t);
                        //found if unique
                        for(var x=0 in char_found){
                            if(char_found[x]==s[z].toLowerCase())
                                w++;
                        }
                        if(w == 0){
                            c++;
                            char_found.push(s[z].toLowerCase())
                        }
                        boldCopy[z] = "<b>"+boldCopy[z]+"</b>";
                       // console.log("found =="+c);
                    }  
                    //console.log("boldCopy[z] ="+boldCopy[z]);
                }
            }
            if(c==text.length){//no char found
                $("#list tr[order='"+j+"']").removeClass("hide");
                //console.log(boldCopy.join(""));
                $("#list").find("tr[order='"+j+"'] .cell_title .text p").html(boldCopy.join(""));
            }
            else{
                $("#list tr[order='"+j+"']").addClass("hide");
            }
        }
    }






